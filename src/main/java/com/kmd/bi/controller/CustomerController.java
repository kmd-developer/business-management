/**
 * 
 */
package com.kmd.bi.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kmd.bi.dto.CustomerDto;
import com.kmd.bi.service.CustomerManagementService;

/**
 * @author KMD
 *
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private CustomerManagementService customerService;

	@GetMapping("/getAll")
	public List<CustomerDto> getAllCustomers() {
		return customerService.findAll();
	}

	@PostMapping("/add")
	public CustomerDto addCustomer(@RequestBody CustomerDto customer) {
		return customerService.save(customer);
	}

	@PutMapping("/update")
	public CustomerDto updateCustomer(@RequestBody CustomerDto customer) {
		return customerService.save(customer);
	}

	@DeleteMapping("/delete")
	public String deleteCustomer(@RequestParam(name = "id", required = false) Long id,
			@RequestParam(name = "name", required = false) String name) {
		if (null == id && null == name) {
			LOGGER.info("At least one request param required to delete an object. please pass ID or Name.");
			return "Please provide id or name";
		} else if (null != id) {
			return customerService.delete(id);
		} else {
			return customerService.delete(name);
		}

	}

}
