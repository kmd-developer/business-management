/**
 * 
 */
package com.kmd.bi.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kmd.bi.dto.ExpenseDto;
import com.kmd.bi.service.ExpenseManagementService;

/**
 * @author KMD
 *
 */
@RestController
@RequestMapping("/expense")
public class ExpenseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExpenseController.class);

	@Autowired
	private ExpenseManagementService expenseService;

	@GetMapping("/getAll")
	public List<ExpenseDto> getAllExpenses() {
		return expenseService.findAll();
	}

	@PostMapping("/add")
	public ExpenseDto addExpense(@RequestBody ExpenseDto expense) {
		return expenseService.save(expense);
	}

	@PutMapping("/update")
	public ExpenseDto updateExpense(@RequestBody ExpenseDto expense) {
		return expenseService.save(expense);
	}

	@DeleteMapping("/delete")
	public String deleteExpense(@RequestParam(name = "id", required = false) Long id,
			@RequestParam(name = "name", required = false) String name) {
		if (null == id && null == name) {
			LOGGER.info("At least one request param required to delete an object. please pass ID or Name.");
			return "Please provide id or name";
		} else if (null != id) {
			return expenseService.delete(id);
		} else {
			return expenseService.delete(name);
		}

	}

}
