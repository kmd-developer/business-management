/**
 * 
 */
package com.kmd.bi.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kmd.bi.dto.ProductDto;
import com.kmd.bi.service.ProductManagementService;

/**
 * @author KMD
 *
 */

@RestController
@RequestMapping("/product")
public class ProductController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductManagementService productService;

	@GetMapping("/getAll")
	public List<ProductDto> getAllProducts() {
		return productService.findAll();
	}

	@PostMapping("/add")
	public ProductDto addProduct(@RequestBody ProductDto product) {
		return productService.save(product);
	}

	@PutMapping("/update")
	public ProductDto updateProduct(@RequestBody ProductDto product) {
		return productService.save(product);
	}

	@DeleteMapping("/delete")
	public String deleteProduct(@RequestParam(name = "id", required = false) Long id,
			@RequestParam(name = "name", required = false) String name) {
		if (null == id && null == name) {
			LOGGER.info("At least one request param required to delete an object. please pass ID or Name.");
			return "Please provide id or name";
		} else if (null != id) {
			return productService.delete(id);
		} else {
			return productService.delete(name);
		}

	}

}
