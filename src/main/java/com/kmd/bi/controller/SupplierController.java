/**
 * 
 */
package com.kmd.bi.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kmd.bi.dto.SupplierDto;
import com.kmd.bi.service.SupplierManagementService;

/**
 * @author KMD
 *
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SupplierController.class);

	@Autowired
	private SupplierManagementService supplierService;

	@GetMapping("/getAll")
	public List<SupplierDto> getAllSuppliers() {
		return supplierService.findAll();
	}

	@PostMapping("/add")
	public SupplierDto addSupplier(@RequestBody SupplierDto supplier) {
		return supplierService.save(supplier);
	}

	@PutMapping("/update")
	public SupplierDto updateSupplier(@RequestBody SupplierDto supplier) {
		return supplierService.save(supplier);
	}

	@DeleteMapping("/delete")
	public String deleteSupplier(@RequestParam(name = "id", required = false) Long id,
			@RequestParam(name = "name", required = false) String name) {
		if (null == id && null == name) {
			LOGGER.info("At least one request param required to delete an object. please pass ID or Name.");
			return "Please provide id or name";
		} else if (null != id) {
			return supplierService.delete(id);
		} else {
			LOGGER.info("Deleting using supplier name");
			return supplierService.delete(name);
		}

	}

}
