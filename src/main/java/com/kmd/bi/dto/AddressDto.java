/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class AddressDto implements Serializable {

	private static final long serialVersionUID = 1297037803169373646L;

	private Long id;
	private String street;
	private String city;
	private String state;
	private String pincode;
	private String country;
	private String email;
	private Long phone;
	private Long mobile;
	private String website;

}
