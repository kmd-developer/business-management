/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class CategoryDto implements Serializable {

	private static final long serialVersionUID = 3629057303204427328L;

	private Long id;
	private String name;

}
