/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class CustomerDto implements Serializable {

	private static final long serialVersionUID = 1383484813486066003L;

	private Long id;
	private String name;
	private String displayName;
	private String companyName;
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private AddressDto billingAddress;
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private AddressDto shippingAddress;
	private String gstType;
	private String gstn;
	private String taxRegistrationNumber;
	private String cstRegistrationNumber;
	private String pan;
	private Boolean applyTDSFlag;
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private TermsDto terms;
}
