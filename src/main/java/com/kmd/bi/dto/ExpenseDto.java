/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class ExpenseDto implements Serializable {

	private static final long serialVersionUID = 2874000066534979663L;

	private Long id;
	private String payeeName;
	private Double amount;
	private Date paymentDate;
	private String paymentMethod;
	private String discription;

}
