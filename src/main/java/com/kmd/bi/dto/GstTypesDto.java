/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class GstTypesDto implements Serializable {

	private static final long serialVersionUID = 7714760056227773702L;

	private Long id;
	private String name;

}
