/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author KMD
 *
 */

@Data
public class ProductDto implements Serializable {

	private static final long serialVersionUID = 5110570348410266563L;

	private Long id;
	private String name;
	private String sku;
	private String hsn;
	private String unit;
	private String unitDisplayName;
	private String category;
	private Long quantity;
	private Long lowStockAlertFlag;
	private String assetType;
	private Double salesPrice;
	private Boolean includSalesTaxFlag;
	private String salesTaxID; // String Refer to tax table
	private Double totalSalesPrice;
	private Double purchasePrice;
	private Boolean includPurchasedTaxFlag;
	private String purchasedTaxID; // String Refer to tax table
	private Double totalPurchasedPrice;
	private String preferredSupplier;

}
