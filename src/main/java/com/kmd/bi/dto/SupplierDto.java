/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kmd.bi.entity.Address;
import com.kmd.bi.entity.Terms;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class SupplierDto implements Serializable {

	private static final long serialVersionUID = 2369667979957978196L;

	private Long id;
	private String name;
	private String displayName;
	private String companyName;
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Address address;
	private String pan;
	private Boolean applyTDSFlag;
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private TdsDto tds;
	private String gstType;
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Terms terms;
	private String gstn;
	private String taxRegistrationNumber;

}
