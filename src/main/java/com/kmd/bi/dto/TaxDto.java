/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class TaxDto implements Serializable {

	private static final long serialVersionUID = -7384348600163304251L;

	private Long id;
	private String name;
	private String type;
	private Long percentage;

}
