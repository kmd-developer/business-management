/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class TdsDto implements Serializable {

	private static final long serialVersionUID = -1257622245600723093L;

	private Long id;
	private String entity;
	private String section;

}
