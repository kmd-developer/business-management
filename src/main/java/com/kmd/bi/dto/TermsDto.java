/**
 * 
 */
package com.kmd.bi.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author KMD
 *
 */
@Data
public class TermsDto implements Serializable {

	private static final long serialVersionUID = -6481717639926614472L;

	private Long id;
	private String name;
	private Integer days;

}
