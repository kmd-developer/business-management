/**
 * 
 */
package com.kmd.bi.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author KMD
 *
 */
@Entity
@Setter
@Getter
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String name;
	private String displayName;
	private String companyName;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Address billingAddress;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Address shippingAddress;
	@NotNull
	private String gstType;
	private String gstn;
	private String taxRegistrationNumber;
	private String cstRegistrationNumber;
	private String pan;
	private Boolean applyTDSFlag;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Terms terms;

}
