/**
 * 
 */
package com.kmd.bi.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author KMD
 *
 */

@Entity
@Getter
@Setter
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String name;
	private String sku;
	private String hsn;
	@NotNull
	private String unit;
	@NotNull
	private String unitDisplayName;
	private String category;
	@NotNull
	private Long quantity;
	private Long lowStockAlertFlag;
	@NotNull
	private String assetType;
	private Double salesPrice;
	private Boolean includSalesTaxFlag;
	private String salesTaxID; // String Refer to tax table
	private Double totalSalesPrice;
	private Double purchasePrice;
	private Boolean includPurchasedTaxFlag;
	private String purchasedTaxID; // String Refer to tax table
	private Double totalPurchasedPrice;
	private String preferredSupplier;

}
