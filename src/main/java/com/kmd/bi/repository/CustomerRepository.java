/**
 * 
 */
package com.kmd.bi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kmd.bi.entity.Customer;

/**
 * @author KMD
 *
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	@Query("select customer from Customer customer where customer.name =:name")
	Customer findByCustomerName(@Param("name") String name);
}
