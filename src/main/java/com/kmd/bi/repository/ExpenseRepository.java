/**
 * 
 */
package com.kmd.bi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kmd.bi.entity.Expense;

/**
 * @author KMD
 *
 */
@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long> {

	@Query("select expense from Expense expense where expense.payeeName =:name")
	Expense findByExpenseName(@Param("name") String name);
}
