/**
 * 
 */
package com.kmd.bi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kmd.bi.entity.Product;

/**
 * @author KMD
 *
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("select product from Product product where product.name =:name")
	Product findByProductName(@Param("name") String name);
}
