/**
 * 
 */
package com.kmd.bi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kmd.bi.entity.Supplier;

/**
 * @author KMD
 *
 */
@Repository
public interface SupplierRespository extends JpaRepository<Supplier, Long> {

	@Query("select supplier from Supplier supplier where supplier.name =:name")
	Supplier findBySupplierName(@Param("name") String name);

}
