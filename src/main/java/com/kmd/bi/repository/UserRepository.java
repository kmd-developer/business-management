/**
 * 
 */
package com.kmd.bi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kmd.bi.entity.User;

/**
 * @author KMD
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
