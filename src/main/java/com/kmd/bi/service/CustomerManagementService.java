/**
 * 
 */
package com.kmd.bi.service;

import java.util.List;

import com.kmd.bi.dto.CustomerDto;

/**
 * @author KMD
 *
 */
public interface CustomerManagementService {

	CustomerDto save(CustomerDto customer);

	List<CustomerDto> findAll();

	CustomerDto findByName(String name);

	String delete(Long id);

	String delete(String name);

}
