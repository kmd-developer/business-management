/**
 * 
 */
package com.kmd.bi.service;

import java.util.List;

import com.kmd.bi.dto.ExpenseDto;

/**
 * @author KMD
 *
 */
public interface ExpenseManagementService {

	ExpenseDto save(ExpenseDto expense);

	List<ExpenseDto> findAll();

	ExpenseDto findByName(String name);

	String delete(Long id);

	String delete(String name);
}
