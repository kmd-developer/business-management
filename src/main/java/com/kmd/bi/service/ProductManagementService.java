/**
 * 
 */
package com.kmd.bi.service;

import java.util.List;

import com.kmd.bi.dto.ProductDto;

/**
 * @author KMD
 *
 */
public interface ProductManagementService {

	ProductDto save(ProductDto product);

	List<ProductDto> findAll();

	ProductDto findByName(String name);

	String delete(Long id);

	String delete(String name);
}
