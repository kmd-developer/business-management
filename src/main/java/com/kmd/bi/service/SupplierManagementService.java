/**
 * 
 */
package com.kmd.bi.service;

import java.util.List;

import com.kmd.bi.dto.SupplierDto;

/**
 * @author KMD
 *
 */
public interface SupplierManagementService {

	SupplierDto save(SupplierDto supplier);

	List<SupplierDto> findAll();

	SupplierDto findByName(String name);

	String delete(Long id);

	String delete(String name);

}
