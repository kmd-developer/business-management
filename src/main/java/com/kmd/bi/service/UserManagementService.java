/**
 * 
 */
package com.kmd.bi.service;

import java.util.List;

import com.kmd.bi.entity.User;

/**
 * @author KMD
 *
 */
public interface UserManagementService {
	
	public List<User> getAllUsers();
	
	public User addUser(String username, String password);

}
