/**
 * 
 */
package com.kmd.bi.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kmd.bi.dto.CustomerDto;
import com.kmd.bi.entity.Customer;
import com.kmd.bi.repository.CustomerRepository;
import com.kmd.bi.service.CustomerManagementService;
import com.kmd.bi.transform.CustomerMapper;

/**
 * @author KMD
 *
 */
@Service
@Transactional
public class CustomerManagementServiceImpl implements CustomerManagementService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerManagementServiceImpl.class);

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public CustomerDto save(CustomerDto customer) {
		if (null != customer) {
			LOGGER.info("Saving customer object...");
			Customer customer2 = CustomerMapper.convertToEntity(customer);
			Customer result = customerRepository.save(customer2);
			LOGGER.info("Customer saved successfully...");
			return CustomerMapper.convertToDto(result);
		}
		return null;

	}

	@Override
	public List<CustomerDto> findAll() {
		LOGGER.info("Get All Customers");
		List<Customer> result = customerRepository.findAll();
		return CustomerMapper.convertToDtoList(result);
	}

	@Override
	public CustomerDto findByName(String name) {
		LOGGER.info("Finding customer by name");
		Customer result = customerRepository.findByCustomerName(name);
		return CustomerMapper.convertToDto(result);
	}

	@Override
	public String delete(Long id) {
		LOGGER.info("Deleting Customer info.");
		customerRepository.deleteById(id);
		return "Customer Deleted";

	}

	@Override
	public String delete(String name) {
		LOGGER.info("Deleting customer having name : {}", name);
		Customer result = customerRepository.findByCustomerName(name);
		if (null != result) {
			delete(result.getId());
		}
		return "Customer Deleted";
	}

}
