/**
 * 
 */
package com.kmd.bi.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kmd.bi.dto.ExpenseDto;
import com.kmd.bi.entity.Expense;
import com.kmd.bi.repository.ExpenseRepository;
import com.kmd.bi.service.ExpenseManagementService;
import com.kmd.bi.transform.ExpenseMapper;

/**
 * @author KMD
 *
 */
@Service
@Transactional
public class ExpenseManagementServiceImpl implements ExpenseManagementService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExpenseManagementServiceImpl.class);

	@Autowired
	private ExpenseRepository expenseRepository;

	@Override
	public ExpenseDto save(ExpenseDto expense) {
		if (null != expense) {
			LOGGER.info("Saving expense object...");
			Expense expense2 = ExpenseMapper.convertToEntity(expense);
			Expense result = expenseRepository.save(expense2);
			LOGGER.info("Expense saved successfully...");
			return ExpenseMapper.convertToDto(result);
		}
		return null;

	}

	@Override
	public List<ExpenseDto> findAll() {
		LOGGER.info("Get All Expenses");
		List<Expense> result = expenseRepository.findAll();
		return ExpenseMapper.convertToDtoList(result);
	}

	@Override
	public ExpenseDto findByName(String name) {
		LOGGER.info("Finding expense by name");
		Expense result = expenseRepository.findByExpenseName(name);
		return ExpenseMapper.convertToDto(result);
	}

	@Override
	public String delete(Long id) {
		LOGGER.info("Deleting Expense info.");
		expenseRepository.deleteById(id);
		return "Expense Deleted";

	}

	@Override
	public String delete(String name) {
		LOGGER.info("Deleting expense having payee name : {}", name);
		Expense result = expenseRepository.findByExpenseName(name);
		if (null != result) {
			delete(result.getId());
		}
		return "Expense Deleted";
	}

}
