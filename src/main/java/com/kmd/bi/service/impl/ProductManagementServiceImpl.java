/**
 * 
 */
package com.kmd.bi.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kmd.bi.dto.ProductDto;
import com.kmd.bi.entity.Product;
import com.kmd.bi.repository.ProductRepository;
import com.kmd.bi.service.ProductManagementService;
import com.kmd.bi.transform.ProductMapper;

/**
 * @author KMD
 *
 */
@Service
public class ProductManagementServiceImpl implements ProductManagementService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductManagementServiceImpl.class);

	@Autowired
	private ProductRepository productRepository;

	@Override
	public ProductDto save(ProductDto product) {
		if (null != product) {
			LOGGER.info("Saving Product...");
			Product product2 = ProductMapper.convertToEntity(product);
			Product result = productRepository.save(product2);
			LOGGER.info("Product details saved successfully...");
			return ProductMapper.convertToDto(result);
		}
		return null;
	}

	@Override
	public List<ProductDto> findAll() {
		LOGGER.info("Getting all products..");
		List<Product> result = productRepository.findAll();
		return ProductMapper.convertToDtoList(result);
	}

	@Override
	public ProductDto findByName(String name) {
		LOGGER.info("Finding product by name.");
		Product result = productRepository.findByProductName(name);
		return ProductMapper.convertToDto(result);
	}

	@Override
	public String delete(Long id) {
		LOGGER.info("Deleting product details using product ID");
		productRepository.deleteById(id);
		return "Deleted";

	}

	@Override
	public String delete(String name) {
		LOGGER.info("Deleting product having product name : {}", name);
		Product result = productRepository.findByProductName(name);
		if (null != result) {
			delete(result.getId());
		}
		return "Product Deleted";
	}

}
