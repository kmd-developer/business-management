/**
 * 
 */
package com.kmd.bi.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kmd.bi.dto.SupplierDto;
import com.kmd.bi.entity.Supplier;
import com.kmd.bi.repository.SupplierRespository;
import com.kmd.bi.service.SupplierManagementService;
import com.kmd.bi.transform.SupplierMapper;

/**
 * @author KMD
 *
 */
@Service
@Transactional
public class SupplierManagementServiceImpl implements SupplierManagementService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SupplierManagementServiceImpl.class);

	@Autowired
	private SupplierRespository supplierRepository;

	@Override
	public SupplierDto save(SupplierDto supplier) {
		if (null != supplier) {
			LOGGER.info("Saving Supplier details.");
			Supplier supplier2 = SupplierMapper.convertToEntity(supplier);
			Supplier result = supplierRepository.save(supplier2);
			LOGGER.info("Supplier details saved successfully");
			return SupplierMapper.convertToDto(result);
		}
		return null;

	}

	@Override
	public List<SupplierDto> findAll() {
		LOGGER.info("Getting all suppliers..");
		List<Supplier> result = supplierRepository.findAll();
		return SupplierMapper.convertToDtoList(result);
	}

	@Override
	public SupplierDto findByName(String name) {
		LOGGER.info("Finding supplier by name..");
		Supplier result = supplierRepository.findBySupplierName(name);
		return SupplierMapper.convertToDto(result);
	}

	@Override
	public String delete(Long id) {
		LOGGER.info("Delete supplier using supplier id.");
		supplierRepository.deleteById(id);
		return "Deleted";

	}

	@Override
	public String delete(String name) {
		LOGGER.info("Deleting supplier having supplier name : {}", name);
		Supplier result = supplierRepository.findBySupplierName(name);
		if (null != result) {
			delete(result.getId());
		}
		return "Supplier Deleted";
	}

}
