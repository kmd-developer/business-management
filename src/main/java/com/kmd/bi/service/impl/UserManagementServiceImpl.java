/**
 * 
 */
package com.kmd.bi.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kmd.bi.entity.User;
import com.kmd.bi.repository.UserRepository;
import com.kmd.bi.service.UserManagementService;

/**
 * @author KMD
 *
 */
@Service
public class UserManagementServiceImpl implements UserManagementService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementServiceImpl.class);

	@Autowired
	public UserRepository userRepository;

	@Override
	public List<User> getAllUsers() {
		LOGGER.info("Get All users..");
		return userRepository.findAll();
	}

	@Override
	public User addUser(String username, String password) {
		LOGGER.info("Adding user to database");
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		return userRepository.save(user);
	}

}
