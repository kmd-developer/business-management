/**
 * 
 */
package com.kmd.bi.transform;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.kmd.bi.dto.CustomerDto;
import com.kmd.bi.entity.Customer;

/**
 * @author KMD
 *
 */
public final class CustomerMapper {
	private static ModelMapper modelMapper = new ModelMapper();

	private CustomerMapper() {

	}

	public static CustomerDto convertToDto(final Customer customer) {
		if (null != customer) {
			return modelMapper.map(customer, CustomerDto.class);
		}
		return null;
	}

	public static Customer convertToEntity(final CustomerDto customerDto) {
		if (null != customerDto) {
			return modelMapper.map(customerDto, Customer.class);
		}
		return null;
	}

	public static List<CustomerDto> convertToDtoList(final List<Customer> customerList) {
		List<CustomerDto> customerDtoList = new ArrayList<>();
		if (null != customerList && !customerList.isEmpty()) {
			for (Customer customer2 : customerList) {
				CustomerDto customerDto = modelMapper.map(customer2, CustomerDto.class);
				customerDtoList.add(customerDto);
			}
			return customerDtoList;
		}
		return customerDtoList;
	}
}
