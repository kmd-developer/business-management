/**
 * 
 */
package com.kmd.bi.transform;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.kmd.bi.dto.ExpenseDto;
import com.kmd.bi.entity.Expense;

/**
 * @author KMD
 *
 */
public final class ExpenseMapper {
	private static ModelMapper modelMapper = new ModelMapper();

	private ExpenseMapper() {

	}

	public static ExpenseDto convertToDto(final Expense expense) {
		if (null != expense) {
			return modelMapper.map(expense, ExpenseDto.class);
		}
		return null;
	}

	public static Expense convertToEntity(final ExpenseDto expenseDto) {
		if (null != expenseDto) {
			return modelMapper.map(expenseDto, Expense.class);
		}
		return null;
	}

	public static List<ExpenseDto> convertToDtoList(final List<Expense> expenseList) {
		List<ExpenseDto> expenseDtoList = new ArrayList<>();
		if (null != expenseList && !expenseList.isEmpty()) {
			for (Expense expense2 : expenseList) {
				ExpenseDto expenseDto = modelMapper.map(expense2, ExpenseDto.class);
				expenseDtoList.add(expenseDto);
			}
			return expenseDtoList;
		}
		return expenseDtoList;
	}
}
