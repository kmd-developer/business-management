/**
 * 
 */
package com.kmd.bi.transform;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.kmd.bi.dto.ProductDto;
import com.kmd.bi.entity.Product;

/**
 * @author KMD
 *
 */
public final class ProductMapper {

	private static ModelMapper modelMapper = new ModelMapper();

	private ProductMapper() {

	}

	public static ProductDto convertToDto(final Product product) {
		if (null != product) {
			return modelMapper.map(product, ProductDto.class);
		}
		return null;
	}

	public static Product convertToEntity(final ProductDto productDto) {
		if (null != productDto) {
			return modelMapper.map(productDto, Product.class);
		}
		return null;
	}

	public static List<ProductDto> convertToDtoList(final List<Product> productList) {
		List<ProductDto> productDtoList = new ArrayList<>();
		if (null != productList && !productList.isEmpty()) {
			for (Product product2 : productList) {
				ProductDto productDto = modelMapper.map(product2, ProductDto.class);
				productDtoList.add(productDto);
			}
			return productDtoList;
		}
		return productDtoList;
	}

}
