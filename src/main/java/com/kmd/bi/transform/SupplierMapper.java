/**
 * 
 */
package com.kmd.bi.transform;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.kmd.bi.dto.SupplierDto;
import com.kmd.bi.entity.Supplier;

/**
 * @author KMD
 *
 */
public final class SupplierMapper {
	private static ModelMapper modelMapper = new ModelMapper();

	private SupplierMapper() {

	}

	public static SupplierDto convertToDto(final Supplier supplier) {
		if (null != supplier) {
			return modelMapper.map(supplier, SupplierDto.class);
		}
		return null;
	}

	public static Supplier convertToEntity(final SupplierDto supplierDto) {
		if (null != supplierDto) {
			return modelMapper.map(supplierDto, Supplier.class);
		}
		return null;
	}

	public static List<SupplierDto> convertToDtoList(final List<Supplier> supplierList) {
		List<SupplierDto> supplierDtoList = new ArrayList<>();
		if (null != supplierList && !supplierList.isEmpty()) {
			for (Supplier supplier2 : supplierList) {
				SupplierDto supplierDto = modelMapper.map(supplier2, SupplierDto.class);
				supplierDtoList.add(supplierDto);
			}
			return supplierDtoList;
		}
		return supplierDtoList;
	}
}
